const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { JWT_SECRET } = require('../config');
const { User } = require('../models/userModel');

module.exports.register = async (req, res) => {
    const { username, password } = req.body;
    try {
        const user = new User({
            username,
            password: await bcrypt.hash(password, 10),
        });

        await user.save();
        res.json({ message: 'User created successfully' });
    } catch (err) {
        res.status(400).json({ message: error.message });
    }
};

module.exports.login = async (req, res) => {
    const { username, password } = req.body;

    const user = await User.findOne({ username });

    if (!user) {
        return res
            .status(400)
            .json({ message: `'${username}' not found!` });
    }

    if (!(await bcrypt.compare(password, user.password))) {
        return res.status(400).json({ message: `Wrong password!` });
    }

    const token = jwt.sign(
        { _id: user._id, username: user.username, createdDate: user.createdDate },
        JWT_SECRET
    );
    res.status(200).json({ message: 'Success', jwt_token: token });
};
