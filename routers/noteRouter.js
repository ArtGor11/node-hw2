const express = require('express');
const router = express.Router();
const { authMiddleWare } = require('./middlewares/authMiddleWare');
const { Note } = require('../models/noteModel');

router.post('/', authMiddleWare, async (req, res) => {
    try {
        const { text } = req.body;
        const note = new Note({
            userId: req.user._id,
            completed: false,
            text: text
        });
        await note.save();
        res.status(200).json({ message: 'Success' });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/', authMiddleWare, async (req, res) => {
    try {
        const { offset = 0, limit = 5 } = req.query;
        const userNotes = await Note.find(
            { userId: req.user._id },
            { __v: 0 },
            {
                skip: parseInt(offset),
                limit: limit > 100 ? 5 : parseInt(limit),
            },
        );
        res.status(200).json({ notes: userNotes });
    } catch (error) {
        res.status(400).json({ message: error.mesage });
    }
});

router.get('/:id', authMiddleWare, async (req, res) => {
    try {
        const userNote = await Note.findOne({
            userId: req.user._id,
            _id: req.params.id,
        });
        res.status(200).json({ note: userNote });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.put('/:id', authMiddleWare, async (req, res) => {
    try {
        await Note.updateOne(
            { userId: req.user._id, _id: req.params.id },
            { text: req.body.text },
        );
        res.status(200).json({ message: 'Success' });
    } catch (err) {
        res.status(400).json({ message: error.message });
    }
});

router.patch('/:id', authMiddleWare, async (req, res) => {
    try {
        const notes = await Note.find({ userId: req.user, _id: req.params.id });
        const bool = notes[0].completed;
        await Note.updateOne(
            { userId: req.user._id, _id: req.params.id },
            { completed: !bool },
        );
        res.status(200).json({ message: 'Success' });
    } catch (err) {
        res.status(400).json({ message: error.message });
    }
});

router.delete('/:id', authMiddleWare, async (req, res) => {
    try {
        await Note.findOneAndDelete({
            _id: req.params.id,
            userId: req.user._id,
        });

        res.status(200).json({ message: 'Success' });
    } catch (err) {
        res.status(400).json({ message: error.message });
    }
});

module.exports = router;
