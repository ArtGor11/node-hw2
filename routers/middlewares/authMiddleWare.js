const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../../config.js');

module.exports.authMiddleWare = async (req, res, next) => {
    const header = req.header('Authorization');

    if (!header) {
        return res.status(400)
            .json({ message: `No Authorization http header found!` });
    }

    const [tokenType, token] = header.split(' ');

    console.log('Token type: ', tokenType);

    if (tokenType !== 'JWT') {
        return res.status(400).json({ message: 'No JWT prefix!' });
    }

    if (!token) {
        return res.status(400).json({ message: 'No token found!' });
    }

    try {
        req.user = jwt.verify(token, JWT_SECRET);
        next();
    } catch (error) {
        return res.status(400).json({ message: 'Bad request' });
    }

};
